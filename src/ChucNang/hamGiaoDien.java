package ChucNang;

import java.awt.Color;
import java.awt.Font;
import java.awt.Insets;
import java.awt.font.TextAttribute;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;

public class hamGiaoDien {
	public void datButtonThanhLabel(JButton btn) {
		btn.setFocusPainted(false);
		btn.setContentAreaFilled(false);
		btn.setOpaque(true);
		btn.setBackground(Color.decode("#c4c4c4"));
		btn.setAlignmentX(JButton.LEFT_ALIGNMENT);
	}
	
	public void datChuGachChan(JLabel lbl) {
		Font font = lbl.getFont();
		Map attributes = font.getAttributes();
		attributes.put(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON);
		lbl.setFont(font.deriveFont(attributes));
	}
	
	public void datChuGachChan(JButton btn) {
		Font font = btn.getFont();
		Map attributes = font.getAttributes();
		attributes.put(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON);
		btn.setFont(font.deriveFont(attributes));
	}
}
