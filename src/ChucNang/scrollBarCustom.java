package ChucNang;

import java.awt.Color;

import javax.swing.JButton;
import javax.swing.JScrollBar;
import javax.swing.LookAndFeel;
import javax.swing.UIManager;
import javax.swing.plaf.basic.BasicArrowButton;
import javax.swing.plaf.basic.BasicScrollBarUI;
//import javax.swing.plaf.metal.MetalScrollBarUI;
public class scrollBarCustom extends BasicScrollBarUI {
	public scrollBarCustom() {
		super();
		thumbHighlightColor = Color.WHITE;
		thumbLightShadowColor = Color.WHITE;
		thumbDarkShadowColor = Color.WHITE;
		thumbColor = Color.WHITE;
		trackColor = Color.WHITE;
		trackHighlightColor = Color.WHITE;
	}
	
	@Override
	protected void configureScrollBarColors()
    {
        LookAndFeel.installColors(scrollbar, "ScrollBar.background",
                                  "ScrollBar.foreground");
    }
	
    protected JButton createDecreaseButton(int orientation)  {
        return new BasicArrowButton(orientation,
                                    Color.RED,
                                    Color.RED,
                                    Color.RED,
                                    Color.RED);
    }

    /**
     * Creates an increase button.
     * @param orientation the orientation
     * @return an increase button
     */
    protected JButton createIncreaseButton(int orientation)  {
        return new BasicArrowButton(orientation,
        		Color.RED,
                Color.RED,
                Color.RED,
                Color.RED);
    }
	
	public JScrollBar getScrollBar() {
		this.scrollbar.setBackground(Color.WHITE);
		return this.scrollbar;
	}
}
