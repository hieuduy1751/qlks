package DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import ENTITY.KhachHang;
import connection.ConnectDB;

public class DAO_KhachHang {
	public ArrayList<KhachHang> getAllPhongBan() {
		ArrayList<KhachHang> dsKH = new ArrayList<KhachHang>();
		try {
			Connection con = ConnectDB.getInstance().getConnection();
			String sql = "select * from KhachHang";
			Statement statement = con.createStatement();
			ResultSet rs = statement.executeQuery(sql);
			while (rs.next()) {
				String maKH = rs.getString(1);
				String hoTen = rs.getString(2);
				String soDienThoai = rs.getString(3);
				String diaChi = rs.getString(4);
				String cmnd = rs.getString(5);
				KhachHang kh = new KhachHang(maKH, hoTen, soDienThoai, diaChi, cmnd);
				dsKH.add(kh);
				
			}
		} catch (SQLException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return dsKH;
	}
	public boolean CapNhat(KhachHang kh) throws SQLException{
		Connection con = ConnectDB.getInstance().getConnection();
		PreparedStatement stmt = null;
		int n =0;
		try {
			stmt = con.prepareStatement("update KhachHang set hoTen = ?, soDienThoai = ?, diaChi =?, cmnd = ? where maKH = ?");
			stmt.setString(1, kh.getHoTen());
			stmt.setString(2, kh.getSoDienThoai());
			stmt.setString(3, kh.getDiaChi());
			stmt.setString(4, kh.getCmnd());
			stmt.setString(5, kh.getMaKH());
			n = stmt.executeUpdate();
		} catch (SQLException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return n>0;
	}
	public boolean delete(String maKH) {
		Connection con = ConnectDB.getInstance().getConnection();
    	PreparedStatement stmt=null;
    	int n = 0;
    	try {
			stmt=con.prepareStatement("delete from KhachHang where maKH = ?");
			stmt.setString(1, maKH);
			n = stmt.executeUpdate();
		} catch (SQLException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
    	return n > 0;
	}
	public ArrayList<KhachHang> timKiemTheoTen(String tim) {
		ArrayList<KhachHang> dsKH = new ArrayList<KhachHang>();
		try {
			Connection con = ConnectDB.getInstance().getConnection();
			String sql = "select * from KhachHang where hoTen like '%" + tim + "%' or maKH like '%" + tim +"%'";
			Statement statement = con.createStatement();
			ResultSet rs = statement.executeQuery(sql);
			while (rs.next()) {
				String maKH = rs.getString(1);
				String hoTen = rs.getString(2);
				String soDienThoai = rs.getString(3);
				String diaChi = rs.getString(4);
				String cmnd = rs.getString(5);
				KhachHang kh = new KhachHang(maKH, hoTen, soDienThoai, diaChi, cmnd);
				dsKH.add(kh);
				
			}
		} catch (SQLException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return dsKH;
	}
}
