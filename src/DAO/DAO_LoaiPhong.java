package DAO;

import ENTITY.LoaiPhong;
import connection.ConnectDB;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class DAO_LoaiPhong {
	private ArrayList<LoaiPhong> DSLoaiPhong;
	
	public DAO_LoaiPhong() {
		DSLoaiPhong = new ArrayList<LoaiPhong>();
        try {
        	Connection con = ConnectDB.getConnection();
            String sql = "select * from dbo.LoaiPhong";
            Statement statement = con.createStatement();
			ResultSet rs = statement.executeQuery(sql);
            while(rs.next()) {
                String maLoai = rs.getString(1);
                String tenLoai = rs.getString(2);
                double donGia = rs.getDouble(3);
                LoaiPhong loai = new LoaiPhong(maLoai.charAt(0), tenLoai, donGia);
                DSLoaiPhong.add(loai);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
	}
    
    public String[] layDSTenLoai() {
    	String[] kq = new String[DSLoaiPhong.size()+1];
    	kq[0] = "Tất cả";
    	for(int i = 1; i <= DSLoaiPhong.size(); i++)
    		kq[i] = DSLoaiPhong.get(i-1).getTenLoai();
    	return kq;
    }

    public String layTenLoai(char maLoai) {
    	for(int i = 0; i < DSLoaiPhong.size(); i++)
    		if(DSLoaiPhong.get(i).getMaLoai() == maLoai)
    			return DSLoaiPhong.get(i).getTenLoai();
		return null;
    }
    
    public char layMaLoai(String tenLoai) {
    	for(int i = 0; i < DSLoaiPhong.size(); i++)
    		if(DSLoaiPhong.get(i).getTenLoai().equals(tenLoai))
    			return DSLoaiPhong.get(i).getMaLoai();
    	return '%';
    }
}
