package DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import ENTITY.NhanVien;
import connection.ConnectDB;

public class DAO_NhanVien {
	public ArrayList<NhanVien> layTatCaNhanVien() {
		ArrayList<NhanVien> dsNhanVien = new ArrayList<NhanVien>();
		try {
			ConnectDB.getInstance();
			Connection conn = ConnectDB.getConnection();

			String sql = "select * from NhanVien";

			PreparedStatement pstm = conn.prepareStatement(sql);
			ResultSet rs = pstm.executeQuery();
			while (rs.next()) {
				String maNV = rs.getString("maNV");
				String hoTen = rs.getString("hoTen");
				String sdt = rs.getString("soDienThoai");
				String cmnd = rs.getString("cmnd");
				String chucVu = rs.getString("ChucVu");
				boolean gioiTinh = (rs.getInt("gioiTinh") == 0) ? false : true;
				dsNhanVien.add(new NhanVien(maNV, hoTen, sdt, cmnd, chucVu, gioiTinh));
			}

		} catch (SQLException e) {
			System.out.println(e);
		}
		return dsNhanVien;
	}
	
	public boolean themNhanVien(NhanVien nv) {
		try {
			Connection conn = ConnectDB.getConnection();
			String sql = "insert into NhanVien values(?,?,?,?,?,?)";
			PreparedStatement pstm = conn.prepareStatement(sql);
			pstm.setString(1, nv.getMaNV());
			pstm.setString(2, nv.getHoTen());
			pstm.setString(3, nv.getSoDienThoai());
			pstm.setString(4, nv.getCmnd());
			pstm.setString(5, nv.getChucVu());
			pstm.setInt(6, nv.isGioiTinh() ? 1 : 0);
			pstm.executeUpdate();
			return true;
		} catch (SQLException e1) {
			System.out.println(e1);
			return false;
		}
	}
	
	public boolean xoaNhanVien(String maNV) {
		try {
			Connection conn = ConnectDB.getConnection();
			String sql = "delete from NhanVien where maNV=?";
			PreparedStatement pstm = conn.prepareStatement(sql);
			pstm.setString(1, maNV);
			pstm.executeUpdate();
			return true;

		} catch (SQLException e1) {
			System.out.println(e1);
			return false;
		}
	}
	
	public boolean suaNhanVien(NhanVien nv) {
		String sql = "update NhanVien set hoten=?,soDienThoai=?,cmnd=?,chucVu=?,gioiTinh=?"
				+ " where maNV=?"	;
		try {
			Connection conn = ConnectDB.getConnection();
			PreparedStatement pstm = conn.prepareStatement(sql);
			pstm.setString(1, nv.getHoTen());
			pstm.setString(2, nv.getSoDienThoai());
			pstm.setString(3, nv.getCmnd());
			pstm.setString(4, nv.getChucVu());
			pstm.setInt(5, nv.isGioiTinh() ? 1 : 0);
			pstm.setString(6, nv.getMaNV());
			pstm.executeUpdate();
			ConnectDB.getInstance().disconnect();
			return true;
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			return false;
		}
	}
	
	public ArrayList<NhanVien> timKiemTheoTen(String tim) {
		ArrayList<NhanVien> dsTimkiem = new ArrayList<NhanVien>();
		try {
			ConnectDB.getInstance();
			Connection con = ConnectDB.getConnection();

			String sql = "select * from NhanVien where hoTen like '%" + tim + "%'or maNV like '%" + tim + "%'";
			Statement statement = con.createStatement();
			ResultSet rs = statement.executeQuery(sql);
			while (rs.next()) {
				String maNV = rs.getString("maNV");
				String hoTen = rs.getString("hoTen");
				String sdt = rs.getString("soDienThoai");
				String cmnd = rs.getString("cmnd");
				String chucVu = rs.getString("ChucVu");
				boolean gioiTinh = (rs.getInt("gioiTinh") == 0) ? false : true;
				dsTimkiem.add(new NhanVien(maNV, hoTen, sdt, cmnd, chucVu, gioiTinh));
			}

		} catch (SQLException e) {
			System.out.println(e);
		}
		return dsTimkiem;
	}
	
	public NhanVien traCuuNhanVien(String maNv) {
		try {
			ConnectDB.getInstance();
			Connection con = ConnectDB.getConnection();

			String sql = "select * from NhanVien where maNV = '" + maNv + "'";
			Statement statement = con.createStatement();
			ResultSet rs = statement.executeQuery(sql);
			while (rs.next()) {
				String maNV = rs.getString("maNV");
				String hoTen = rs.getString("hoTen");
				String sdt = rs.getString("soDienThoai");
				String cmnd = rs.getString("cmnd");
				String chucVu = rs.getString("ChucVu");
				boolean gioiTinh = (rs.getInt("gioiTinh") == 0) ? false : true;
				return new NhanVien(maNV, hoTen, sdt, cmnd, chucVu, gioiTinh);
			}

		} catch (SQLException e) {
			System.out.println(e);
		}
		return null;
	}
}
