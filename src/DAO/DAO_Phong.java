package DAO;

import ENTITY.Phong;
import connection.ConnectDB;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Hashtable;

public class DAO_Phong {
    private Hashtable<Integer, Phong> DSPhong;
    private DAO_LoaiPhong dao_loaiPhong = new DAO_LoaiPhong();
    public DAO_Phong() {
        DSPhong = new Hashtable<Integer, Phong>();
        try {
        	Connection con = ConnectDB.getConnection();
            String sql = "select * from dbo.Phong";
            Statement statement = con.createStatement();
			ResultSet rs = statement.executeQuery(sql);
            int i = 0;
            while(rs.next()) {
            	String maPhong = rs.getString(1);
                boolean tinhTrang = rs.getBoolean(2);
                String thongTin = rs.getString(3);
                char maLoai = rs.getString(4).charAt(0);
                int tang = rs.getInt(5);
                Phong phong = new Phong(maPhong, tinhTrang, thongTin, maLoai, tang);
                DSPhong.put(i, phong);
                i++;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
    
    public Hashtable<Integer, Phong> layDSPhong() {
        return this.DSPhong;
    }

    public Object[] layDSTang() {
    	ArrayList<String> dstang = new ArrayList<String>();
    	dstang.add("Tất cả");
    	for(int i = 0; i < DSPhong.size(); i++)
    		if(!dstang.contains(Integer.toString(DSPhong.get(i).getTang())))
    			dstang.add(Integer.toString(DSPhong.get(i).getTang()));
    	return dstang.toArray();
    }  
    
    public void timKiemDSPhong(String timkiem, String tangTK, String loaiTK, String trangthaiTK) {
    	DSPhong = new Hashtable<Integer, Phong>();
    	int trangthai;
    	if(trangthaiTK == "Trống") {
    		trangthai = 0;
    	}
    	else 
    		trangthai = 1;
    	try {
    		Connection con = ConnectDB.getConnection();
            String sql = "select * from dbo.Phong where Phong.maPhong like '%" + timkiem + "%'"
            			+ (tangTK == "Tất cả" ? "" : (" and Phong.tang like " + tangTK))
            			+ (trangthaiTK == "Tất cả" ? "" : (" and Phong.tinhTrang like " + trangthai))
    					+ (loaiTK == "Tất cả" ? "" : (" and Phong.maLoai like '" + dao_loaiPhong.layMaLoai(loaiTK) +"'"));
            System.out.println(sql);
            Statement statement = con.createStatement();
			ResultSet rs = statement.executeQuery(sql);
            int i = 0;
            while(rs.next()) {
            	String maPhong = rs.getString(1);
                boolean tinhTrang = rs.getBoolean(2);
                String thongTin = rs.getString(3);
                char maLoai = rs.getString(4).charAt(0);
                int tang = rs.getInt(5);
                Phong phong = new Phong(maPhong, tinhTrang, thongTin, maLoai, tang);
                DSPhong.put(i, phong);
                i++;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
}
