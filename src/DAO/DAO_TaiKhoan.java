package DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import ENTITY.NhanVien;
import ENTITY.Phong;
import ENTITY.TaiKhoan;
import connection.ConnectDB;

public class DAO_TaiKhoan {
	
	private ArrayList<TaiKhoan> dsTK;
	
	public ArrayList<TaiKhoan> layDSTaiKhoan() {
		dsTK = new ArrayList<TaiKhoan>();
		try {
        	Connection con = ConnectDB.getConnection();
            String sql = "select taiKhoan, maNV from dbo.TaiKhoan";
            Statement statement = con.createStatement();
			ResultSet rs = statement.executeQuery(sql);
            while(rs.next()) {
            	String tenDN = rs.getString(1);
                String matKhau = null;
                String maNV = rs.getString(2);
                TaiKhoan taiKhoan = new TaiKhoan(tenDN, matKhau, maNV);
                dsTK.add(taiKhoan);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
		return dsTK;
	}
	
	public static NhanVien DangNhap(String taiKhoan, String matKhau) {
		String maNV = null;
		try {
			Connection con = ConnectDB.getConnection();
			String sql = "select maNV from dbo.TaiKhoan where taiKhoan = '" + taiKhoan + "' and matKhau = '" + matKhau + "'";
			Statement statement = con.createStatement();
			ResultSet rs = statement.executeQuery(sql);
            while(rs.next()) {
            	maNV = rs.getString(1);
            }
		} catch (SQLException e) {
			e.printStackTrace();
		}
		if(maNV == null)
			return null;
		else {
			try {
				Connection con = ConnectDB.getConnection();
				String sql = "select * from dbo.NhanVien where maNV = '" + maNV + "'";
				Statement statement = con.createStatement();
				ResultSet rs = statement.executeQuery(sql);
	            while(rs.next()) {
	            	String ma = rs.getString(1);
	            	String hoTen = rs.getString(2);
	            	String soDienThoai = rs.getString(3);
	            	String cmnd = rs.getString(4);
	            	String chucVu = rs.getString(5);
	            	boolean gioiTinh = rs.getBoolean(6);
	            	NhanVien nv = new NhanVien(ma, hoTen, soDienThoai, cmnd, chucVu, gioiTinh);
	            	return nv;
	            }
			} catch (SQLException e) {
				e.printStackTrace();
				return null;
			}
		}
		return null;
	}
	
	public boolean kiemTraCoTK(String maNV) {
		int i = 0;
		try {
			ConnectDB.getInstance();
			Connection con = ConnectDB.getConnection();
			String sql = "select * from dbo.TaiKhoan where maNV = '" + maNV + "'";
			Statement statement = con.createStatement();
			ResultSet rs = statement.executeQuery(sql);
			
			while(rs.next())
				i++;

		} catch (SQLException e) {
			e.printStackTrace();
		}
		if(i > 0)
			return true;
		return false;
	}

	public boolean taoTK(String tk, String mk, NhanVien nv) {
		try {
			Connection conn = ConnectDB.getConnection();
			String sql = "insert into taiKhoan values(?,?,?)";
			PreparedStatement pstm = conn.prepareStatement(sql);
			pstm.setString(1, tk);
			pstm.setString(2, mk);
			pstm.setString(3, nv.getMaNV());
			pstm.executeUpdate();
			return true;
		} catch (SQLException e1) {
			System.out.println(e1);
			return false;
		}
		
	}
}
