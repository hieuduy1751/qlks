package ENTITY;

import java.util.Objects;

public class LoaiPhong {
    private char maLoai;
    private String tenLoai;
    private double donGia;

    public LoaiPhong(char maLoai, String tenLoai, double donGia) {
        this.maLoai = maLoai;
        this.tenLoai = tenLoai;
        this.donGia = donGia;
    }

    public char getMaLoai() {
        return maLoai;
    }

    public void setMaLoai(char maLoai) {
        this.maLoai = maLoai;
    }

    public String getTenLoai() {
        return tenLoai;
    }

    public void setTenLoai(String tenLoai) {
        this.tenLoai = tenLoai;
    }

    public double getDonGia() {
        return donGia;
    }

    public void setDonGia(double donGia) {
        this.donGia = donGia;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LoaiPhong loaiPhong = (LoaiPhong) o;
        return maLoai == loaiPhong.maLoai;
    }

    @Override
    public int hashCode() {
        return Objects.hash(maLoai);
    }
}
