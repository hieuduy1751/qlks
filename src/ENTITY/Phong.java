package ENTITY;

import java.util.Objects;

public class Phong {
    private String maPhong;
    private boolean tinhTrang;
    private String thongTin;
    private char maLoai;
    private int tang;

    public Phong(String maPhong, boolean tinhTrang, String thongTin, char maLoai, int tang) {
        this.maPhong = maPhong;
        this.tinhTrang = tinhTrang;
        this.thongTin = thongTin;
        this.maLoai = maLoai;
        this.tang = tang;
    }

    public String getMaPhong() {
        return maPhong;
    }

    public void setMaPhong(String maPhong) {
        this.maPhong = maPhong;
    }

    public boolean isTinhTrang() {
        return tinhTrang;
    }

    public void setTinhTrang(boolean tinhTrang) {
        this.tinhTrang = tinhTrang;
    }

    public String getThongTin() {
        return thongTin;
    }

    public void setThongTin(String thongTin) {
        this.thongTin = thongTin;
    }

    public char getMaLoai() {
        return maLoai;
    }

    public void setMaLoai(char maLoai) {
        this.maLoai = maLoai;
    }

    public int getTang() {
        return tang;
    }

    public void setTang(int tang) {
        this.tang = tang;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Phong phong = (Phong) o;
        return maPhong.equals(phong.maPhong);
    }

    @Override
    public int hashCode() {
        return Objects.hash(maPhong);
    }
}
