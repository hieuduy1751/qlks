package ENTITY;

public class TaiKhoan {
	private String taiKhoan;
	private String matKhau;
	private String maNV;
	public TaiKhoan(String taiKhoan, String matKhau, String maNV) {
		super();
		this.taiKhoan = taiKhoan;
		this.matKhau = matKhau;
		this.maNV = maNV;
	}
	public String getTaiKhoan() {
		return taiKhoan;
	}
	public void setTaiKhoan(String taiKhoan) {
		this.taiKhoan = taiKhoan;
	}
	public String getMatKhau() {
		return matKhau;
	}
	public void setMatKhau(String matKhau) {
		this.matKhau = matKhau;
	}
	public String getMaNV() {
		return maNV;
	}
	public void setMaNV(String maNV) {
		this.maNV = maNV;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((taiKhoan == null) ? 0 : taiKhoan.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TaiKhoan other = (TaiKhoan) obj;
		if (taiKhoan == null) {
			if (other.taiKhoan != null)
				return false;
		} else if (!taiKhoan.equals(other.taiKhoan))
			return false;
		return true;
	}
	
	
}
