package GUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.SQLException;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import DAO.DAO_KhachHang;
import ENTITY.KhachHang;
import connection.ConnectDB;

public class GUI_DSKH implements ActionListener, MouseListener {
	private JPanel DSkhPanel = new JPanel();
	private JLabel lblTieuDe;
	private JLabel lblMaKH;
	private JTextField txtMaKH;
	private JTextField txtTen;
	private JLabel lblTen;
	private JLabel lblSDT;
	private JTextField txtSDT;
	private JLabel lblDiaChi;
	private JTextField txtDiaChi;
	private JLabel lblCMND;
	private JTextField txtCMND;
	private JLabel lblRegex;
	private JButton btnXoa;
	private JButton btnSua;
	private JTextField txtTimKiem;
	private DefaultTableModel tableModel;
	private JTable table;
	private JButton btnTimKiem;
	private JScrollPane scrl;
	private DAO_KhachHang dao_dsKH = new DAO_KhachHang();
	public GUI_DSKH() {
		DSkhPanel = new JPanel();
		DSkhPanel.setPreferredSize(new Dimension(700, 500));
		DSkhPanel.setBackground(Color.WHITE);
		JPanel pnNorth= new JPanel();
		JPanel pnSouth= new JPanel();
		pnNorth.setBackground(Color.WHITE);
		pnSouth.setBackground(Color.WHITE);
		
		lblTieuDe= new JLabel("DANH SÁCH KHÁCH HÀNG",JLabel.CENTER);
		
		txtTimKiem= new JTextField(10);
		
		btnXoa= new JButton("Xóa");
		btnSua= new JButton("Sửa thông tin");
		btnTimKiem= new JButton("Tìm");
		
		String[] header = {"Mã Khách Hàng", "Họ Tên", "Số điện thoại", "Địa chỉ", "Số CMND"};
		tableModel= new DefaultTableModel(header,0) {
			@Override
		    public boolean isCellEditable(int row, int column) {
		       //all cells false
		       return false;
		    }
		};
		table = new JTable(tableModel);
		scrl= new JScrollPane(table);
		scrl.setPreferredSize(new Dimension(660, 550));
		table.setOpaque(true);
		table.setFillsViewportHeight(true);
		table.setBackground(Color.WHITE);
		table.getTableHeader().setFont(new Font("Arial", Font.BOLD, 15));
		table.getTableHeader().setOpaque(false);
		table.getTableHeader().setBackground(Color.decode("#c4c4c4"));
		table.getTableHeader().setBorder(BorderFactory.createLineBorder(Color.WHITE));
		TableColumnModel columnModel = table.getColumnModel();
//		table.setShowGrid(false);
		table.setShowVerticalLines(false);
		table.setGridColor(Color.decode("#c4c4c4"));
		table.setRowHeight(25);
		table.setSelectionBackground(Color.decode("#c4c4c4"));
		scrl.setBackground(Color.WHITE);
		scrl.setBorder(BorderFactory.createLineBorder(Color.decode("#c4c4c4")));
		scrl.getVerticalScrollBar().setOpaque(true);
		scrl.getVerticalScrollBar().setBackground(Color.WHITE);
		scrl.getVerticalScrollBar().setBorder(BorderFactory.createLineBorder(Color.WHITE));
		columnModel.getColumn(0).setPreferredWidth(50);
		columnModel.getColumn(1).setPreferredWidth(100);
		columnModel.getColumn(2).setPreferredWidth(50);
		columnModel.getColumn(3).setPreferredWidth(60);

		btnXoa.setFocusPainted(false);
		btnSua.setFocusPainted(false);
		btnTimKiem.setFocusPainted(false);
		
		txtTimKiem.setPreferredSize(btnSua.getPreferredSize());
		txtTimKiem.setBackground(Color.decode("#c4c4c4"));
		txtTimKiem.setBorder(BorderFactory.createEmptyBorder());
		
		btnXoa.setBackground(Color.decode("#c4c4c4"));
		btnSua.setBackground(Color.decode("#c4c4c4"));
		btnTimKiem.setBackground(Color.decode("#c4c4c4"));
		
		
		lblTieuDe.setFont(new Font("arial", Font.BOLD, 20));
		
		
		pnNorth.setLayout(new BorderLayout());
		
		
		
		pnNorth.add(lblTieuDe,BorderLayout.NORTH);
		lblTieuDe.setBorder(BorderFactory.createEmptyBorder(20, 0, 10, 0));
		
		pnSouth.add(txtTimKiem);
		pnSouth.add(btnTimKiem);
		pnSouth.add(btnSua);
		pnSouth.add(btnXoa);
		
		DSkhPanel.add(pnNorth,BorderLayout.NORTH);
		DSkhPanel.add(scrl,BorderLayout.CENTER);
		DSkhPanel.add(pnSouth,BorderLayout.SOUTH);
		try {
			ConnectDB.getInstance().connect();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
        updateTableData();
		
		
		
        btnSua.addActionListener(new ActionListener() {

			private JButton btnLuu;
			private JButton btnHuy;
			

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				if(table.getSelectedRow() != -1) {

					JFrame editFrame = new JFrame();
					editFrame.setTitle("Chỉnh sửa thông tin khách hàng");
					editFrame.setForeground(Color.WHITE);
					editFrame.setSize(500, 340);
					editFrame.setVisible(true);
					editFrame.setLocationRelativeTo(null);
					JPanel pEdit = new JPanel();
					JPanel p1 = new JPanel();
					JPanel p2 = new JPanel();
					JPanel p3 = new JPanel();
					JPanel p4 = new JPanel();
					JPanel p5 = new JPanel();
					JPanel p6 = new JPanel();
					JPanel p11 = new JPanel();
					
					p1.add(lblMaKH = new JLabel("Mã Khách Hàng"));
					p1.add(txtMaKH = new JTextField(20));
					txtMaKH.setEditable(false);
					lblRegex = new JLabel();
					lblRegex.setFont(new Font("Arial", Font.ITALIC, 15));
					lblRegex.setForeground(Color.RED);
					p2.add(lblTen = new JLabel("Họ Tên"));
					p2.add(txtTen = new JTextField(20));
					p3.add(lblSDT = new JLabel("Số điện thoại"));
					p3.add(txtSDT =new JTextField(20));
					p4.add(lblDiaChi = new JLabel("Địa chỉ"));
					p4.add(txtDiaChi = new JTextField(20));
					p5.add(lblCMND = new JLabel("Số CMND"));
					p5.add(txtCMND =new JTextField(20));
					p6.add(lblRegex);
					p11.add(btnLuu = new JButton("Lưu"));
					p11.add(btnHuy = new JButton("Hủy"));
					btnLuu.setBackground(Color.decode("#c4c4c4"));
					btnHuy.setBackground(Color.decode("#c4c4c4"));
					
					p1.setPreferredSize(new Dimension(400, 40));
					p2.setPreferredSize(new Dimension(400, 40));
					p3.setPreferredSize(new Dimension(400, 40));
					p4.setPreferredSize(new Dimension(400, 40));
					p5.setPreferredSize(new Dimension(400, 40));
					p6.setPreferredSize(new Dimension(400, 30));

					lblTen.setPreferredSize(lblMaKH.getPreferredSize());
					lblSDT.setPreferredSize(lblMaKH.getPreferredSize());
					lblDiaChi.setPreferredSize(lblMaKH.getPreferredSize());
					lblCMND.setPreferredSize(lblMaKH.getPreferredSize());
					
					pEdit.add(p1);
					pEdit.add(p2);
					pEdit.add(p3);
					pEdit.add(p4);
					pEdit.add(p5);
					pEdit.add(p6);
					pEdit.add(p11);
					
					editFrame.add(pEdit);
					showData();
					btnLuu.addActionListener(new ActionListener() {
						
						@Override
						public void actionPerformed(ActionEvent e) {
							// TODO Auto-generated method stub
								int row = table.getSelectedRow();
								if(regex()) {
									KhachHang kh = new KhachHang(txtMaKH.getText(), txtTen.getText(), txtSDT.getText(), txtDiaChi.getText(), txtCMND.getText());	
									try {
										if(dao_dsKH.CapNhat(kh)) {
											System.out.println(kh);
											table.setValueAt(txtTen.getText(), row, 1);
											table.setValueAt(txtSDT.getText(), row, 2);
											table.setValueAt(txtDiaChi.getText(), row, 3);
											table.setValueAt(txtCMND.getText(), row, 4);
											editFrame.dispose();
										}
									} catch (Exception e2) {
										// TODO: handle exception
										e2.printStackTrace();
									}
								}
						}
					});
					btnHuy.addActionListener(new ActionListener() {
						
						@Override
						public void actionPerformed(ActionEvent e) {
							// TODO Auto-generated method stub
							editFrame.dispose();
						}
					});
				} else {
					JOptionPane.showMessageDialog(DSkhPanel, "Phải chọn khách hàng trước", "Sửa thông tin khách hàng", JOptionPane.WARNING_MESSAGE);
				}
			}
        });
        table.addMouseListener(this);
        btnXoa.addActionListener(this);
        btnTimKiem.addActionListener(this);
	}
	
	 private void updateTableData() {
			List<KhachHang> listKH = dao_dsKH.getAllPhongBan();
			for(KhachHang s : listKH) {
				String[] rowData= {s.getMaKH(),s.getHoTen(),s.getSoDienThoai(),s.getDiaChi(),s.getCmnd()};
				tableModel.addRow(rowData);
			}
			table.setModel(tableModel);
		}
	@Override
	public void actionPerformed(ActionEvent e1) {
		// TODO Auto-generated method stub
		Object obj1 = e1.getSource();
		if(obj1.equals(btnXoa)) {
			int row= table.getSelectedRow();
			if(row != -1) {
				int temp2 = JOptionPane.showConfirmDialog(DSkhPanel, "Xác nhận xóa", "Xác nhận",
						JOptionPane.YES_NO_OPTION);
				if (temp2 == JOptionPane.YES_OPTION) {
					if(dao_dsKH.delete((String) table.getValueAt(row,0))) {
						tableModel.removeRow(row);
						JOptionPane.showMessageDialog(DSkhPanel, "Xóa khách hàng thành công", "Thành công", JOptionPane.INFORMATION_MESSAGE);
					} else {
						JOptionPane.showMessageDialog(DSkhPanel, "Xóa khách hàng thất bại", "Thất bại", JOptionPane.ERROR_MESSAGE);
					}
				}
			} else {
				JOptionPane.showMessageDialog(DSkhPanel, "Phải chọn khách hàng trước", "Xóa khách hàng", JOptionPane.WARNING_MESSAGE);
			}
		}if(obj1.equals(btnTimKiem)) {
			tableModel.setRowCount(0);
			String nd = txtTimKiem.getText();
			if(nd.length() == 0){
				List<KhachHang> listKH = dao_dsKH.getAllPhongBan();
				for(KhachHang s : listKH) {
					String[] rowData= {s.getMaKH(),s.getHoTen(),s.getSoDienThoai(),s.getDiaChi(),s.getCmnd()};
					tableModel.addRow(rowData);
				}
				table.setModel(tableModel);
				}else {
					List<KhachHang> listKH = dao_dsKH.timKiemTheoTen(nd);
					for(KhachHang s : listKH) {
						String[] rowData= {s.getMaKH(),s.getHoTen(),s.getSoDienThoai(),s.getDiaChi(),s.getCmnd()};
						tableModel.addRow(rowData);
					}
					table.setModel(tableModel);
			}
		}
	}
	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
	}
	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	
	public void showData() {
			int row=table.getSelectedRow();
			txtMaKH.setText(table.getValueAt(row, 0).toString());
			txtTen.setText(table.getValueAt(row, 1).toString());
			txtSDT.setText(table.getValueAt(row, 2).toString());
			txtDiaChi.setText(table.getValueAt(row, 3).toString());
			txtCMND.setText(table.getValueAt(row, 4).toString());
			
	}
	public boolean regex() {
		String maKH = txtMaKH.getText().trim();
		String hoTen = txtTen.getText().trim();
		String soDienThoai = txtSDT.getText().trim();
		String diaChi = txtDiaChi.getText().trim();
		String cmnd = txtCMND.getText().trim();
		if(hoTen.length() > 0) {
			if(!(hoTen.matches("^[A-Za-z ]+$"))) {
				lblRegex.setText("Họ tên nhập sai định dạng");
				return false;
			}
		}else {
			lblRegex.setText("Họ tên không được để trống");
			return false;
		}
		if(soDienThoai.length() > 0) {
			if(!(soDienThoai.matches("^\\d{10}$"))){
				lblRegex.setText("Số điện thoại phải nhập đúng định dạng");
				return false;
			}
		}else {
			lblRegex.setText("Số điện thoại không được để trống");
			return false;
		}
		if(diaChi.length() > 0) {
			if(!(diaChi.matches("[\\w ]+$"))){
				lblRegex.setText("Địa chỉ phải nhập đúng đinh dạng");
				return false;
			}
		}else {
			lblRegex.setText("Địa chỉ không được để trống");
			return false;
		}
		
		if(cmnd.length() > 0) {
			if(!(cmnd.matches("^\\d{9,12}"))) {
				lblRegex.setText("Số CMND phải nhập đúng 9-12 số");
				return false;
			}
		}else {
			lblRegex.setText("Số CMND không được để trống");
			return false;
		}
		lblRegex.setText("");
		return true;
	} 
	public JPanel getDSkhPanel() {
		return this.DSkhPanel;
	}
}
