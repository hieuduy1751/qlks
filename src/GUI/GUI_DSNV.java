package GUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;

import DAO.DAO_NhanVien;
import ENTITY.NhanVien;

public class GUI_DSNV implements ActionListener {
	private DAO_NhanVien dao_nv = new DAO_NhanVien();
	private JPanel DSnvPanel;
	private DefaultTableModel tableModel;
	private JTable table;
	private JScrollPane scrl;
	private JLabel lblTieuDe;
	private JButton btnXoa;
	private JButton btnThem;
	private JButton btnSua;
	private JTextField txtTimKiem;
	private JButton btnTim;
	private JLabel lblMaNV;
	private JTextField txtMaNV;
	private JLabel lblRegex;
	private JLabel lblTen;
	private JTextField txtTen;
	private JLabel lblSDT;
	private JTextField txtSDT;
	private JLabel lblCMND;
	private JTextField txtCMND;
	private JLabel lblChucVu;
	private JComboBox cbxChucVu;
	private JLabel lblGioiTinh;
	private JCheckBox chkGioiTinh;
	private String[] col= {"Mã Nhân Viên","Tên nhân viên","Số điện thoại","CMND","Chức vụ","Giới tính"};

	public GUI_DSNV() {
		DSnvPanel = new JPanel();
		DSnvPanel.setPreferredSize(new Dimension(700, 500));
		DSnvPanel.setBackground(Color.WHITE);
		JPanel pnNorth= new JPanel();
		JPanel pnSouth= new JPanel();
		pnNorth.setBackground(Color.WHITE);
		pnSouth.setBackground(Color.WHITE);
		lblTieuDe= new JLabel("THÔNG TIN NHÂN VIÊN",JLabel.CENTER);
		
		txtTimKiem= new JTextField(10);
		
		btnXoa= new JButton("Xóa");
		btnThem= new JButton("Thêm");
		btnSua= new JButton("Sửa thông tin");
		btnTim= new JButton("Tìm");
		tableModel= new DefaultTableModel(col,0) {
			@Override
		    public boolean isCellEditable(int row, int column) {
		       //all cells false
		       return false;
		    }
		};
		table = new JTable(tableModel);
		scrl= new JScrollPane(table);
		scrl.setPreferredSize(new Dimension(660, 550));
		table.setOpaque(true);
		table.setFillsViewportHeight(true);
		table.setBackground(Color.WHITE);
		table.getTableHeader().setFont(new Font("Arial", Font.BOLD, 15));
		table.getTableHeader().setOpaque(false);
		table.getTableHeader().setBackground(Color.decode("#c4c4c4"));
		table.getTableHeader().setBorder(BorderFactory.createLineBorder(Color.WHITE));
		TableColumnModel columnModel = table.getColumnModel();
//		table.setShowGrid(false);
		table.setShowVerticalLines(false);
		table.setGridColor(Color.decode("#c4c4c4"));
		table.setRowHeight(25);
		table.setSelectionBackground(Color.decode("#c4c4c4"));
		scrl.setBackground(Color.WHITE);
		scrl.setBorder(BorderFactory.createLineBorder(Color.decode("#c4c4c4")));
		scrl.getVerticalScrollBar().setOpaque(true);
		scrl.getVerticalScrollBar().setBackground(Color.WHITE);
		scrl.getVerticalScrollBar().setBorder(BorderFactory.createLineBorder(Color.WHITE));
		columnModel.getColumn(0).setPreferredWidth(50);
		columnModel.getColumn(1).setPreferredWidth(100);
		columnModel.getColumn(2).setPreferredWidth(50);
		columnModel.getColumn(3).setPreferredWidth(60);
		columnModel.getColumn(4).setPreferredWidth(40);
		columnModel.getColumn(5).setPreferredWidth(20);
		
		btnXoa.setFocusPainted(false);
		btnSua.setFocusPainted(false);
		btnThem.setFocusPainted(false);
		btnTim.setFocusPainted(false);
		
		txtTimKiem.setPreferredSize(btnSua.getPreferredSize());
		txtTimKiem.setBackground(Color.decode("#c4c4c4"));
		txtTimKiem.setBorder(BorderFactory.createEmptyBorder());
		
		btnXoa.setBackground(Color.decode("#c4c4c4"));
		btnSua.setBackground(Color.decode("#c4c4c4"));
		btnThem.setBackground(Color.decode("#c4c4c4"));
		btnTim.setBackground(Color.decode("#c4c4c4"));
		
		
		lblTieuDe.setFont(new Font("arial", Font.BOLD, 20));
		
		
		pnNorth.setLayout(new BorderLayout());
		
		pnNorth.add(lblTieuDe,BorderLayout.NORTH);
		lblTieuDe.setBorder(BorderFactory.createEmptyBorder(20, 0, 10, 0));
		
		pnSouth.add(txtTimKiem);
		pnSouth.add(btnTim);
		pnSouth.add(btnThem);
		pnSouth.add(btnSua);
		pnSouth.add(btnXoa);
		
		DSnvPanel.add(pnNorth,BorderLayout.NORTH);
		DSnvPanel.add(scrl,BorderLayout.CENTER);
		DSnvPanel.add(pnSouth,BorderLayout.SOUTH);
		
		updateTableData();
		
		btnThem.addActionListener(new ActionListener() {

			private JButton btnThem;
			private JButton btnHuy;
			

			@Override
			public void actionPerformed(ActionEvent e) {
					JFrame editFrame = new JFrame();
					editFrame.setTitle("Thêm nhân viên");
					editFrame.setForeground(Color.WHITE);
					editFrame.setSize(500, 400);
					editFrame.setVisible(true);
					editFrame.setLocationRelativeTo(null);
					JPanel pEdit = new JPanel();
					JPanel p1 = new JPanel();
					JPanel p2 = new JPanel();
					JPanel p3 = new JPanel();
					JPanel p4 = new JPanel();
					JPanel p5 = new JPanel();
					JPanel p6 = new JPanel();
					JPanel p7 = new JPanel();
					JPanel p11 = new JPanel();
					
					p1.add(lblMaNV = new JLabel("Mã Nhân Viên"));
					p1.add(txtMaNV = new JTextField(20));
					lblRegex = new JLabel();
					lblRegex.setFont(new Font("Arial", Font.ITALIC, 15));
					lblRegex.setForeground(Color.RED);
					p2.add(lblTen = new JLabel("Họ Tên"));
					p2.add(txtTen = new JTextField(20));
					p3.add(lblSDT = new JLabel("Số điện thoại"));
					p3.add(txtSDT =new JTextField(20));
					p4.add(lblCMND = new JLabel("Số CMND"));
					p4.add(txtCMND =new JTextField(20));
					p5.add(lblChucVu = new JLabel("Chức vụ"));
					p5.add(cbxChucVu = new JComboBox(new String[] {"Nhân viên", "Quản lý"}));
					p6.add(lblGioiTinh = new JLabel("Giới tính: "));
					p6.add(chkGioiTinh = new JCheckBox("Nam"));
					p7.add(lblRegex);
					p11.add(btnThem = new JButton("Thêm"));
					p11.add(btnHuy = new JButton("Hủy"));
					btnThem.setBackground(Color.decode("#c4c4c4"));
					btnHuy.setBackground(Color.decode("#c4c4c4"));
					
					p1.setPreferredSize(new Dimension(400, 40));
					p2.setPreferredSize(new Dimension(400, 40));
					p3.setPreferredSize(new Dimension(400, 40));
					p4.setPreferredSize(new Dimension(400, 40));
					p5.setPreferredSize(new Dimension(400, 40));
					p6.setPreferredSize(new Dimension(400, 40));
					p7.setPreferredSize(new Dimension(400, 30));

					lblTen.setPreferredSize(lblMaNV.getPreferredSize());
					lblSDT.setPreferredSize(lblMaNV.getPreferredSize());
					lblChucVu.setPreferredSize(lblMaNV.getPreferredSize());
					lblCMND.setPreferredSize(lblMaNV.getPreferredSize());
					
					pEdit.add(p1);
					pEdit.add(p2);
					pEdit.add(p3);
					pEdit.add(p4);
					pEdit.add(p5);
					pEdit.add(p6);
					pEdit.add(p7);
					pEdit.add(p11);
					
					editFrame.add(pEdit);
					btnThem.addActionListener(new ActionListener() {
						
						@Override
						public void actionPerformed(ActionEvent e) {
								if(kiemTraTrungMa() && kiemTraNhapLieu()) {
									NhanVien nv = new NhanVien(txtMaNV.getText(), txtTen.getText(), txtSDT.getText(),txtCMND.getText(), cbxChucVu.getSelectedItem().toString(), chkGioiTinh.isSelected());
									if(dao_nv.themNhanVien(nv)) {
										tableModel.addRow(new Object[] {txtMaNV.getText(), txtTen.getText(), txtSDT.getText(),txtCMND.getText(), cbxChucVu.getSelectedItem().toString(), chkGioiTinh.isSelected() ? "Nam" : "Nữ"});
										editFrame.dispose();
										JOptionPane.showMessageDialog(DSnvPanel, "Thêm nhân viên thành công", "Thành công", JOptionPane.INFORMATION_MESSAGE);
									} else {
										JOptionPane.showMessageDialog(DSnvPanel, "Thêm nhân viên thất bại", "Thất bại", JOptionPane.ERROR_MESSAGE);
									}
								}
						}
					});
					btnHuy.addActionListener(new ActionListener() {
						
						@Override
						public void actionPerformed(ActionEvent e) {
							// TODO Auto-generated method stub
							editFrame.dispose();
						}
					});
			}
        });
		btnSua.addActionListener(new ActionListener() {

			private JButton btnLuu;
			private JButton btnHuy;
		
			@Override
			public void actionPerformed(ActionEvent e) {
					if(table.getSelectedRow() != -1) {
						JFrame editFrame = new JFrame();
						editFrame.setTitle("Sửa thông tin nhân viên");
						editFrame.setForeground(Color.WHITE);
						editFrame.setSize(500, 400);
						editFrame.setVisible(true);
						editFrame.setLocationRelativeTo(null);
						JPanel pEdit = new JPanel();
						JPanel p1 = new JPanel();
						JPanel p2 = new JPanel();
						JPanel p3 = new JPanel();
						JPanel p4 = new JPanel();
						JPanel p5 = new JPanel();
						JPanel p6 = new JPanel();
						JPanel p7 = new JPanel();
						JPanel p11 = new JPanel();
						
						p1.add(lblMaNV = new JLabel("Mã Nhân Viên"));
						p1.add(txtMaNV = new JTextField(20));
						txtMaNV.setEditable(false);
						lblRegex = new JLabel();
						lblRegex.setFont(new Font("Arial", Font.ITALIC, 15));
						lblRegex.setForeground(Color.RED);
						p2.add(lblTen = new JLabel("Họ Tên"));
						p2.add(txtTen = new JTextField(20));
						p3.add(lblSDT = new JLabel("Số điện thoại"));
						p3.add(txtSDT =new JTextField(20));
						p4.add(lblCMND = new JLabel("Số CMND"));
						p4.add(txtCMND =new JTextField(20));
						p5.add(lblChucVu = new JLabel("Chức vụ"));
						p5.add(cbxChucVu = new JComboBox(new String[] {"Nhân viên", "Quản lý"}));
						p6.add(lblGioiTinh = new JLabel("Giới tính: "));
						p6.add(chkGioiTinh = new JCheckBox("Nam"));
						p7.add(lblRegex);
						p11.add(btnLuu = new JButton("Sửa"));
						p11.add(btnHuy = new JButton("Hủy"));
						btnLuu.setBackground(Color.decode("#c4c4c4"));
						btnHuy.setBackground(Color.decode("#c4c4c4"));
						
						p1.setPreferredSize(new Dimension(400, 40));
						p2.setPreferredSize(new Dimension(400, 40));
						p3.setPreferredSize(new Dimension(400, 40));
						p4.setPreferredSize(new Dimension(400, 40));
						p5.setPreferredSize(new Dimension(400, 40));
						p6.setPreferredSize(new Dimension(400, 40));
						p7.setPreferredSize(new Dimension(400, 30));

						lblTen.setPreferredSize(lblMaNV.getPreferredSize());
						lblSDT.setPreferredSize(lblMaNV.getPreferredSize());
						lblChucVu.setPreferredSize(lblMaNV.getPreferredSize());
						lblCMND.setPreferredSize(lblMaNV.getPreferredSize());
						
						pEdit.add(p1);
						pEdit.add(p2);
						pEdit.add(p3);
						pEdit.add(p4);
						pEdit.add(p5);
						pEdit.add(p6);
						pEdit.add(p7);
						pEdit.add(p11);
						showData();
						editFrame.add(pEdit);
						btnLuu.addActionListener(new ActionListener() {
							
							@Override
							public void actionPerformed(ActionEvent e) {
									if(kiemTraNhapLieu()) {
										NhanVien nv = new NhanVien(txtMaNV.getText(), txtTen.getText(), txtSDT.getText(),txtCMND.getText(), cbxChucVu.getSelectedItem().toString(), chkGioiTinh.isSelected());
										if(dao_nv.suaNhanVien(nv)) {
											int row = table.getSelectedRow();
											table.setValueAt(txtTen.getText(), row, 1);
											table.setValueAt(txtSDT.getText(), row, 2);
											table.setValueAt(txtCMND.getText(), row, 3);
											table.setValueAt(cbxChucVu.getSelectedItem().toString(), row, 4);
											table.setValueAt(chkGioiTinh.isSelected() ? "Nam" : "Nữ", row, 5);
											editFrame.dispose();
											JOptionPane.showMessageDialog(DSnvPanel, "Cập nhật thông tin nhân viên thành công", "Thành công", JOptionPane.INFORMATION_MESSAGE);
										} else {
											JOptionPane.showMessageDialog(DSnvPanel, "Cập nhật thông tin nhân viên thất bại", "Thất bại", JOptionPane.ERROR_MESSAGE);
										}
									}
							}
						});
						btnHuy.addActionListener(new ActionListener() {
							
							@Override
							public void actionPerformed(ActionEvent e) {
								// TODO Auto-generated method stub
								editFrame.dispose();
							}
						});
					} else {
						JOptionPane.showMessageDialog(DSnvPanel, "Phải chọn nhân viên trước", "Sửa thông tin nhân viên", JOptionPane.WARNING_MESSAGE);
					}
			}
        });
        btnXoa.addActionListener(this);
        btnTim.addActionListener(this);
	}
	
	public JPanel getDSnvPanel() {
		return this.DSnvPanel;
	}

	
	
	private void updateTableData() {
		List<NhanVien> listNV = dao_nv.layTatCaNhanVien();
		for(NhanVien s : listNV) {
			String[] rowData= {s.getMaNV(), s.getHoTen(), s.getSoDienThoai(), s.getCmnd(), s.getChucVu(), s.isGioiTinh() ? "Nam" : "Nữ"};
			tableModel.addRow(rowData);
		}
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		Object ob = e.getSource();
		if (ob.equals(btnXoa)) {
			int temp = table.getSelectedRow();
			if(temp != -1) {
				int temp2 = JOptionPane.showConfirmDialog(DSnvPanel, "Xác nhận xóa", "Xác nhận",
						JOptionPane.YES_NO_OPTION);
				if (temp2 == JOptionPane.YES_OPTION) {
					if(dao_nv.xoaNhanVien((String) table.getValueAt(temp,0))) {
						tableModel.removeRow(temp);
						JOptionPane.showMessageDialog(DSnvPanel, "Xóa nhân viên thành công", "Thành công", JOptionPane.INFORMATION_MESSAGE);
					} else {
						JOptionPane.showMessageDialog(DSnvPanel, "Xóa nhân viên thất bại", "Thất bại", JOptionPane.ERROR_MESSAGE);
					}
				}
			} else {
				JOptionPane.showMessageDialog(DSnvPanel, "Phải chọn nhân viên trước", "Xóa nhân viên", JOptionPane.WARNING_MESSAGE);
			}
		}
		if(ob.equals(btnTim)) {
			tableModel.setRowCount(0);
			String nd = txtTimKiem.getText();
			if(nd.length() == 0){
				List<NhanVien> listNV = dao_nv.layTatCaNhanVien();
				for(NhanVien s : listNV) {
					String[] rowData= {s.getMaNV(),s.getHoTen(),s.getSoDienThoai(),s.getCmnd(),s.getChucVu(),s.isGioiTinh() ? "Nam" : "Nữ"};
					tableModel.addRow(rowData);
				}
				table.setModel(tableModel);
				}else {
					List<NhanVien> listNV = dao_nv.timKiemTheoTen(nd);
					for(NhanVien s : listNV) {
						String[] rowData= {s.getMaNV(),s.getHoTen(),s.getSoDienThoai(),s.getCmnd(),s.getChucVu(),s.isGioiTinh() ? "Nam" : "Nữ"};
						tableModel.addRow(rowData);
					}
					table.setModel(tableModel);
			}
		}
	}
	
	public boolean kiemTraNhapLieu() {
		
		String maNV = txtMaNV.getText().trim();
		String hoTen = txtTen.getText().trim();
		String sdt = txtSDT.getText().trim();
		String cmnd = txtCMND.getText().trim();
		if (maNV.equals("") || !maNV.matches("(NV|QL)\\d{3}")) {
			focus(txtMaNV, "Bắt đầu bằng NV or QL theo sau là 3 số.");
			return false;
		}
		if (hoTen.equals("") || !hoTen.matches("\\w+(\\s\\w+)*")) {
			focus(txtTen, "tên gồm 1 hoặc nhiều từ cách nhau bởi khoảng trắng");
			return false;
		}
		if (sdt.equals("") || !sdt.matches("\\d{10}")) {
			focus(txtSDT, "Số điện thoại gồm 10 chữ số");
			return false;
		}
		if (cmnd.equals("") || !cmnd.matches("((\\d{9})|(\\d{12}))")) {
			focus(txtCMND, "CMND/CCCD gồm 9 hoặc 12 chữ số");
			return false;
		}
		return true;
	}
	
	public boolean kiemTraTrungMa() {
		ArrayList<NhanVien> dsNhanVien = dao_nv.layTatCaNhanVien();
		NhanVien nv = new NhanVien(txtMaNV.getText().trim(), txtTen.getText().trim(),
				txtSDT.getText().trim(), txtCMND.getText().trim(), cbxChucVu.getSelectedItem().toString().trim(),
				chkGioiTinh.isSelected() ? true : false);
		if (dsNhanVien.contains(nv)) {
			focus(txtMaNV, "Trùng mã");
			return false;
		} else
			return true;
	}
	
	public void focus(JTextField txt, String loi) {
		txt.requestFocus();
		txt.selectAll();
		lblRegex.setText(loi);
	}
	
	public void showData() {
		int row=table.getSelectedRow();
		txtMaNV.setText(table.getValueAt(row, 0).toString());
		txtTen.setText(table.getValueAt(row, 1).toString());
		txtSDT.setText(table.getValueAt(row, 2).toString());
		txtCMND.setText(table.getValueAt(row, 3).toString());
		cbxChucVu.setSelectedIndex(table.getValueAt(row, 4).toString() == cbxChucVu.getSelectedItem() ? 0 : 1);
		chkGioiTinh.setSelected(table.getValueAt(row, 5).toString() == "Nam" ? true : false);
	}
}
