package GUI;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.Hashtable;

import javax.swing.*;

import DAO.DAO_LoaiPhong;
import DAO.DAO_Phong;
import ENTITY.Phong;
import connection.ConnectDB;

public class GUI_DSphong implements ActionListener, KeyListener {

	private JComboBox cbxTang;
	private JComboBox cbxTrangThai;
	private JComboBox cbxLoaiPhong;
	private JTextField txtTimKiem;
	private DAO_LoaiPhong dao_loaiphong = new DAO_LoaiPhong();
	private DAO_Phong dao_phong = new DAO_Phong();
	private Hashtable<Integer, Phong> DSPhong = this.dao_phong.layDSPhong();
	private ArrayList<JButton> DSbtnPhanTrang;
	private Hashtable<Integer, JButton> DSbtnPhong;
	private JPanel DSphongPanel;
	private JPanel phongCenter;
	private JPanel phongSouth;

	public GUI_DSphong() {
		DSphongPanel = new JPanel();
		this.PhongCenter(1);
		this.PhongSouth();
		DSphongPanel.add(this.PhongNorth(), BorderLayout.NORTH);
		DSphongPanel.add(this.phongCenter, BorderLayout.CENTER);
		DSphongPanel.add(this.phongSouth, BorderLayout.SOUTH);
		DSphongPanel.setBorder(BorderFactory.createMatteBorder(20, 30, 30, 30, Color.WHITE));
		DSphongPanel.setBackground(Color.WHITE);	
	}
	
	public JPanel getDSphongPanel() {
		return DSphongPanel;
	}
	
	public JPanel PhongNorth() {
		JPanel phongNorth = new JPanel(null);
		phongNorth.setPreferredSize(new Dimension(640, 60));
		phongNorth.setBackground(Color.WHITE);

		JLabel lblTimKiem = new JLabel("Tìm kiếm phòng");
		JLabel lblTang = new JLabel("Tầng");
		JLabel lblTrangThai = new JLabel("Trạng thái");
		JLabel lblLoaiPhong = new JLabel("Loại phòng");

		txtTimKiem = new JTextField(5);
		cbxTang = new JComboBox(dao_phong.layDSTang());
		cbxTrangThai = new JComboBox(new String[] {"Tất cả", "Trống", "Đang sử dụng"});
		cbxLoaiPhong = new JComboBox(dao_loaiphong.layDSTenLoai());
		
		txtTimKiem.setSize(lblTimKiem.getPreferredSize());
		cbxTang.setPreferredSize(lblTang.getPreferredSize());
		cbxLoaiPhong.setPreferredSize(lblLoaiPhong.getPreferredSize());
		cbxTrangThai.setPreferredSize(lblTrangThai.getPreferredSize());
		
		txtTimKiem.setBackground(Color.decode("#c4c4c4"));
		txtTimKiem.setBorder(BorderFactory.createEmptyBorder());
		
		cbxTang.setBackground(Color.decode("#c4c4c4"));
		cbxTang.setBorder(BorderFactory.createEmptyBorder());
		
		cbxLoaiPhong.setBackground(Color.decode("#c4c4c4"));
		cbxLoaiPhong.setBorder(BorderFactory.createEmptyBorder());
		
		cbxTrangThai.setBackground(Color.decode("#c4c4c4"));
		cbxTrangThai.setBorder(BorderFactory.createEmptyBorder());
		
		lblTimKiem.setFont(new Font("Arial", Font.PLAIN, 20));
		lblTang.setFont(new Font("Arial", Font.PLAIN, 20));
		lblTrangThai.setFont(new Font("Arial", Font.PLAIN, 20));
		lblLoaiPhong.setFont(new Font("Arial", Font.PLAIN, 20));

		lblTimKiem.setBounds(0, 0, 150, 24);
		lblTang.setBounds(170, 0, 70, 24);
		lblLoaiPhong.setBounds(270, 0, 100, 24);
		lblTrangThai.setBounds(390, 0, 100, 24);
		txtTimKiem.setBounds(0, 30, 150, 24);
		cbxTang.setBounds(170, 30, 70, 24);
		cbxLoaiPhong.setBounds(270, 30, 100, 24);
		cbxTrangThai.setBounds(390, 30, 120, 24);

		phongNorth.add(lblTimKiem);
		phongNorth.add(lblTang);
		phongNorth.add(lblLoaiPhong);
		phongNorth.add(lblTrangThai);
		phongNorth.add(txtTimKiem);
		phongNorth.add(cbxTang);
		phongNorth.add(cbxLoaiPhong);
		phongNorth.add(cbxTrangThai);
		
		txtTimKiem.addKeyListener(this);
		cbxTang.addActionListener(this);
		cbxLoaiPhong.addActionListener(this);
		cbxTrangThai.addActionListener(this);

		return phongNorth;
	}

	public void PhongCenter(int trang) {
		phongCenter = new JPanel(new GridLayout(4, 4, 50, 20));
		phongCenter.setPreferredSize(new Dimension(640, 520));
		phongCenter.setBorder(BorderFactory.createMatteBorder(10, 0, 0, 0, Color.WHITE));
		phongCenter.setBackground(Color.WHITE);
		phongCenter.removeAll();
		DSbtnPhong = new Hashtable<Integer, JButton>();

		for(int i = (trang - 1) * 16; i < 16 * trang; i++) {
			if(i < DSPhong.size()) {
				DSbtnPhong.put(i, this.taoOPhong(DSPhong.get(i)));
				DSbtnPhong.get(i).addActionListener(this);
				phongCenter.add(DSbtnPhong.get(i));
			}
			else {
				JPanel panel = new JPanel();
				panel.setBackground(Color.WHITE);
				phongCenter.add(panel);
			}
		}
		phongCenter.revalidate();
		phongCenter.repaint();
	}

	public void PhongSouth() {
		phongSouth = new JPanel();
		phongSouth.setPreferredSize(new Dimension(640, 40));
		phongSouth.setBackground(Color.WHITE);
		DSbtnPhanTrang = new ArrayList<JButton>();
		for(int i = 1; i <= ((DSPhong.size()%16 == 0) ? DSPhong.size()/16 : (DSPhong.size()/16)+1); i++) {
			DSbtnPhanTrang.add(this.taoTrangBtn(i));
			DSbtnPhanTrang.get(i-1).addActionListener(this);
			phongSouth.add(DSbtnPhanTrang.get(i-1));
		}
	}

	public JButton taoOPhong(Phong phong) {
		JButton btnPhong = new JButton();
		JLabel lblPhong = new JLabel(phong.getMaPhong());
		JLabel lblTang = new JLabel("Tầng " + Integer.toString(phong.getTang()));
		JLabel lblLoai = new JLabel("Phòng " + dao_loaiphong.layTenLoai(phong.getMaLoai()));

		if(phong.isTinhTrang() == false)
			btnPhong.setBackground(Color.decode("#33AB80"));
//		else if(trangThai == 1)
//			btnPhong.setBackground(Color.decode("#E3BF63"));
		else
			btnPhong.setBackground(Color.decode("#AB3333"));
		
		btnPhong.setPreferredSize(new Dimension(125, 125));
		btnPhong.setBorder(BorderFactory.createLineBorder(Color.decode("#c4c4c4"), 3));
		btnPhong.setMargin(new Insets(5, 5, 5, 5));
		lblPhong.setFont(new Font("Arial", Font.BOLD, 25));
		lblTang.setFont(new Font("Arial", Font.PLAIN, 12));
		lblLoai.setFont(new Font("Arial", Font.PLAIN, 14));
		lblPhong.setForeground(Color.BLACK);
		lblTang.setForeground(Color.BLACK);
		lblLoai.setForeground(Color.BLACK);
		lblPhong.setAlignmentX(JLabel.CENTER_ALIGNMENT);
		lblTang.setAlignmentX(JLabel.CENTER_ALIGNMENT);
		lblLoai.setAlignmentX(JLabel.CENTER_ALIGNMENT);
		lblTang.setBorder(BorderFactory.createEmptyBorder(10,0,10,0));

		Box verticalBox = Box.createVerticalBox();

		verticalBox.add(lblPhong);
		verticalBox.add(lblTang);
		verticalBox.add(lblLoai);
		
		btnPhong.add(verticalBox);
		return btnPhong;
	}

	public JButton taoTrangBtn(int trang) {
		JButton btn = new JButton(Integer.toString(trang));
		btn.setFocusPainted(false);
		btn.setMargin(new Insets(5, 5, 5, 5));
		btn.setPreferredSize(new Dimension(25, 25));
		btn.setBackground(Color.decode("#c4c4c4"));
//		btn.setBorder(BorderFactory.createEmptyBorder());
		return btn;
	}
	
	public void taoLaiPanel() {
		this.DSPhong = this.dao_phong.layDSPhong();
		this.DSphongPanel.remove(this.phongCenter);
		this.DSphongPanel.remove(this.phongSouth);
		this.PhongCenter(1);
		this.PhongSouth();
		this.DSphongPanel.add(this.phongCenter, BorderLayout.CENTER);
		this.DSphongPanel.add(this.phongSouth);
		this.DSphongPanel.revalidate();
		this.DSphongPanel.repaint();
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		Object source = e.getSource();
		if(source.equals(cbxTang) || source.equals(cbxLoaiPhong) || source.equals(cbxTrangThai)) {
			this.dao_phong.timKiemDSPhong(txtTimKiem.getText(), cbxTang.getSelectedItem().toString(), cbxLoaiPhong.getSelectedItem().toString(), cbxTrangThai.getSelectedItem().toString());
			this.taoLaiPanel();
		}
		for(int i = 0; i < DSbtnPhanTrang.size(); i++) {
			if(source.equals(DSbtnPhanTrang.get(i))) {
				this.DSphongPanel.remove(this.phongCenter);
				this.DSphongPanel.remove(this.phongSouth);
				this.PhongCenter(Integer.parseInt(DSbtnPhanTrang.get(i).getText()));
				this.DSphongPanel.add(this.phongCenter, BorderLayout.CENTER);
				this.DSphongPanel.add(this.phongSouth);
				this.DSphongPanel.revalidate();
				this.DSphongPanel.repaint();
			}
		}
		DSbtnPhong.forEach((k,v) -> {
			if(source.equals(v)) {
				System.out.println(DSPhong.get(k).getMaPhong());
			}
		});
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		this.dao_phong.timKiemDSPhong(txtTimKiem.getText(), cbxTang.getSelectedItem().toString(), cbxLoaiPhong.getSelectedItem().toString(), cbxTrangThai.getSelectedItem().toString());
		this.taoLaiPanel();
	}
}
