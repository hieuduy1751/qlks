package GUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import DAO.DAO_TaiKhoan;
import ENTITY.NhanVien;

public class GUI_DangNhap implements ActionListener {
	private JPanel gui_dangnhap = new JPanel(new BorderLayout());
	private NhanVien nhanVien;
	private JLabel lblDangNhap;
	private JLabel lblTenDN;
	private JLabel lblMatKhau;
	private JTextField txtTenDN;
	private JPasswordField txtMatKhau;
	private JButton btnDangNhap;
	private JLabel lblThongBao;
	private JPanel khoangTrong;
	
	public GUI_DangNhap() {
		gui_dangnhap.setPreferredSize(new Dimension(1000, 700));
		gui_dangnhap.setBackground(Color.WHITE);
		Dimension expectedDimension = new Dimension(550, 440);

		
		JPanel panel = new JPanel();
        panel.setPreferredSize(expectedDimension);
        panel.setMaximumSize(expectedDimension);
        panel.setMinimumSize(expectedDimension);
        
        lblDangNhap = new JLabel("ĐĂNG NHẬP", JLabel.CENTER);
        lblTenDN = new JLabel("Tên đăng nhập");
        lblMatKhau = new JLabel("Mật khẩu");
        txtTenDN = new JTextField();
        txtMatKhau = new JPasswordField();
        btnDangNhap = new JButton("XÁC THỰC");

        lblDangNhap.setFont(new Font("Arial", Font.BOLD, 48));		
        lblTenDN.setFont(new Font("Arial", Font.PLAIN, 30));
        lblMatKhau.setFont(new Font("Arial", Font.PLAIN, 30));
        
        lblTenDN.setPreferredSize(new Dimension(550, 40));
		lblMatKhau.setPreferredSize(new Dimension(550, 40));
        txtTenDN.setPreferredSize(new Dimension(550, 60));
        txtMatKhau.setPreferredSize(new Dimension(550, 60));
        
        txtTenDN.setBackground(Color.decode("#c4c4c4"));
		txtTenDN.setBorder(BorderFactory.createEmptyBorder());
		txtMatKhau.setBackground(Color.decode("#c4c4c4"));
		txtMatKhau.setBorder(BorderFactory.createEmptyBorder());
		txtTenDN.setFont(new Font("Arial", Font.PLAIN, 35));
		txtMatKhau.setFont(new Font("Arial", Font.PLAIN, 35));
		lblDangNhap.setBorder(BorderFactory.createEmptyBorder(0, 0, 30, 0));
		khoangTrong = new JPanel();
		khoangTrong.setPreferredSize(new Dimension(550, 50));
		khoangTrong.setBackground(Color.WHITE);
		lblThongBao = new JLabel("Sai tên đăng nhập hoặc mật khẩu");
		lblThongBao.setForeground(Color.RED);
		
		btnDangNhap.setFocusPainted(false);
		btnDangNhap.setMargin(new Insets(5, 5, 5, 5));
		btnDangNhap.setPreferredSize(new Dimension(150, 50));
		btnDangNhap.setBackground(Color.decode("#c4c4c4"));
		
		
        panel.setBackground(Color.WHITE);
        panel.add(lblDangNhap);
        panel.add(lblTenDN);
        panel.add(txtTenDN);
        panel.add(lblMatKhau);
        panel.add(txtMatKhau);
        panel.add(khoangTrong);
        panel.add(btnDangNhap);
        
        Box box = new Box(BoxLayout.Y_AXIS);

        box.add(Box.createVerticalGlue());
        box.add(panel);     
        box.add(Box.createVerticalGlue());
        
        gui_dangnhap.add(box);
        
        btnDangNhap.addActionListener(this);
        
	}
	
	public JPanel getGUIdangNhap() {
		return this.gui_dangnhap;
	}
	
	public NhanVien getNV() {
		return this.nhanVien;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(DAO_TaiKhoan.DangNhap(txtTenDN.getText(), txtMatKhau.getText()) == null) {
			khoangTrong.add(lblThongBao);
			gui_dangnhap.revalidate();
			gui_dangnhap.repaint();
		} else {
			nhanVien = DAO_TaiKhoan.DangNhap(txtTenDN.getText(), txtMatKhau.getText());
			khoangTrong.removeAll();
			gui_dangnhap.revalidate();
			gui_dangnhap.repaint();
		}
		
		 Component source = (Component) e.getSource();
		    while (source.getParent() != null) {            
		        source = source.getParent();
		    }

		    if (source instanceof ActionListener) {
		      ((ActionListener) source).actionPerformed(e);
		    }
		  }
}
