package GUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.sql.SQLException;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;

import DAO.DAO_NhanVien;
import DAO.DAO_TaiKhoan;
import ENTITY.NhanVien;
import ENTITY.TaiKhoan;
import connection.ConnectDB;

public class GUI_QLTaiKhoan {
	private DAO_TaiKhoan dao_tk = new DAO_TaiKhoan();
	private DAO_NhanVien dao_nv = new DAO_NhanVien();
	private JPanel QLTKPanel;
	private JLabel lblTieuDe;
	private JTextField txtTen;
	private JLabel lblTen;
	private JLabel lblSDT;
	private JTextField txtSDT;
	private JLabel lblCMND;
	private JTextField txtCMND;
	private JLabel lblRegex;
	private JButton btnCapTk;
	private JButton btnXoa;
	private JButton btnSua;
	private JTextField txtTimKiem;
	private DefaultTableModel tableModel;
	private JTable table;
	private JButton btnTimKiem;
	private JScrollPane scrl;
	
	private JLabel lblMaNV;
	private JTextField txtMaNV;
	private JLabel lblChucVu;
	private JTextField txtChucVu;
	private JLabel lblTK;
	private JTextField txtTK;
	private JLabel lblMK;
	private JPasswordField txtMK;
	private JLabel lblNMK;
	private JPasswordField txtNMK;
	
	public GUI_QLTaiKhoan() {
		QLTKPanel = new JPanel();
		QLTKPanel.setPreferredSize(new Dimension(700, 500));
		QLTKPanel.setBackground(Color.WHITE);
		JPanel pnNorth= new JPanel();
		JPanel pnSouth= new JPanel();
		pnNorth.setBackground(Color.WHITE);
		pnSouth.setBackground(Color.WHITE);
		
		lblTieuDe= new JLabel("DANH SÁCH TÀI KHOẢN",JLabel.CENTER);
		
		txtTimKiem= new JTextField(10);
		
		btnXoa= new JButton("Xóa");
		btnSua= new JButton("Sửa thông tin");
		btnCapTk= new JButton("Thêm");
		btnTimKiem= new JButton("Tìm");
		
		String[] header = {"Mã nhân viên", "Tài khoản", "Nhân viên", "Chức vụ"};
		tableModel= new DefaultTableModel(header,0) {
			@Override
		    public boolean isCellEditable(int row, int column) {
		       //all cells false
		       return false;
		    }
		};
		table = new JTable(tableModel);
		scrl= new JScrollPane(table);
		scrl.setPreferredSize(new Dimension(660, 550));
		table.setOpaque(true);
		table.setFillsViewportHeight(true);
		table.setBackground(Color.WHITE);
		table.getTableHeader().setFont(new Font("Arial", Font.BOLD, 15));
		table.getTableHeader().setOpaque(false);
		table.getTableHeader().setBackground(Color.decode("#c4c4c4"));
		table.getTableHeader().setBorder(BorderFactory.createLineBorder(Color.WHITE));
		TableColumnModel columnModel = table.getColumnModel();
//		table.setShowGrid(false);
		table.setShowVerticalLines(false);
		table.setGridColor(Color.decode("#c4c4c4"));
		table.setRowHeight(25);
		table.setSelectionBackground(Color.decode("#c4c4c4"));
		scrl.setBackground(Color.WHITE);
		scrl.setBorder(BorderFactory.createLineBorder(Color.decode("#c4c4c4")));
		scrl.getVerticalScrollBar().setOpaque(true);
		scrl.getVerticalScrollBar().setBackground(Color.WHITE);
		scrl.getVerticalScrollBar().setBorder(BorderFactory.createLineBorder(Color.WHITE));
		columnModel.getColumn(0).setPreferredWidth(50);
		columnModel.getColumn(1).setPreferredWidth(50);
		columnModel.getColumn(2).setPreferredWidth(100);
		columnModel.getColumn(3).setPreferredWidth(60);

		btnXoa.setFocusPainted(false);
		btnSua.setFocusPainted(false);
		btnCapTk.setFocusPainted(false);
		btnTimKiem.setFocusPainted(false);
		
		txtTimKiem.setPreferredSize(btnSua.getPreferredSize());
		txtTimKiem.setBackground(Color.decode("#c4c4c4"));
		txtTimKiem.setBorder(BorderFactory.createEmptyBorder());
		
		btnXoa.setBackground(Color.decode("#c4c4c4"));
		btnSua.setBackground(Color.decode("#c4c4c4"));
		btnCapTk.setBackground(Color.decode("#c4c4c4"));
		btnTimKiem.setBackground(Color.decode("#c4c4c4"));
		
		
		lblTieuDe.setFont(new Font("arial", Font.BOLD, 20));
		
		
		pnNorth.setLayout(new BorderLayout());
		
		
		
		pnNorth.add(lblTieuDe,BorderLayout.NORTH);
		lblTieuDe.setBorder(BorderFactory.createEmptyBorder(20, 0, 10, 0));
		
		pnSouth.add(txtTimKiem);
		pnSouth.add(btnTimKiem);
		pnSouth.add(btnCapTk);
		pnSouth.add(btnSua);
		pnSouth.add(btnXoa);
		
		QLTKPanel.add(pnNorth,BorderLayout.NORTH);
		QLTKPanel.add(scrl,BorderLayout.CENTER);
		QLTKPanel.add(pnSouth,BorderLayout.SOUTH);
		try {
			ConnectDB.getInstance().connect();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		updateTableData();
		
		btnCapTk.addActionListener(new ActionListener() {

			private JButton btnThem;
			private JButton btnHuy;
			boolean kiemTra;

		
			@Override
			public void actionPerformed(ActionEvent e) {
						JFrame editFrame = new JFrame();
						editFrame.setTitle("Sửa thông tin nhân viên");
						editFrame.setForeground(Color.WHITE);
						editFrame.setSize(500, 400);
						editFrame.setVisible(true);
						editFrame.setLocationRelativeTo(null);
						JPanel pEdit = new JPanel();
						JPanel p1 = new JPanel();
						JPanel p2 = new JPanel();
						JPanel p3 = new JPanel();
						JPanel p4 = new JPanel();
						JPanel p5 = new JPanel();
						JPanel p6 = new JPanel();
						JPanel p7 = new JPanel();
						JPanel p11 = new JPanel();
						
						p1.add(lblMaNV = new JLabel("Mã Nhân Viên"));
						p1.add(txtMaNV = new JTextField(20));
						lblRegex = new JLabel();
						lblRegex.setFont(new Font("Arial", Font.ITALIC, 15));
						lblRegex.setForeground(Color.RED);
						p2.add(lblTen = new JLabel("Họ Tên"));
						p2.add(txtTen = new JTextField(20));
						p3.add(lblChucVu = new JLabel("Chức vụ"));
						p3.add(txtChucVu = new JTextField(20));
						p4.add(lblTK = new JLabel("Tài khoản"));
						p4.add(txtTK =new JTextField(20));
						p5.add(lblMK = new JLabel("Mật khẩu"));
						p5.add(txtMK =new JPasswordField(20));
						p6.add(lblNMK = new JLabel("Nhập lại"));
						p6.add(txtNMK =new JPasswordField(20));
						p7.add(lblRegex);
						p11.add(btnThem = new JButton("Cấp"));
						p11.add(btnHuy = new JButton("Hủy"));
						btnThem.setBackground(Color.decode("#c4c4c4"));
						btnHuy.setBackground(Color.decode("#c4c4c4"));
						
						p1.setPreferredSize(new Dimension(400, 40));
						p2.setPreferredSize(new Dimension(400, 40));
						p3.setPreferredSize(new Dimension(400, 40));
						p4.setPreferredSize(new Dimension(400, 40));
						p5.setPreferredSize(new Dimension(400, 40));
						p6.setPreferredSize(new Dimension(400, 40));
						p7.setPreferredSize(new Dimension(400, 30));

						lblTen.setPreferredSize(lblMaNV.getPreferredSize());
						lblChucVu.setPreferredSize(lblMaNV.getPreferredSize());
						lblTK.setPreferredSize(lblMaNV.getPreferredSize());
						lblMK.setPreferredSize(lblMaNV.getPreferredSize());
						lblNMK.setPreferredSize(lblMaNV.getPreferredSize());
						
						txtTen.setEditable(false);
						txtChucVu.setEditable(false);
						
						pEdit.add(p1);
						pEdit.add(p2);
						pEdit.add(p3);
						pEdit.add(p4);
						pEdit.add(p5);
						pEdit.add(p6);
						pEdit.add(p7);
						pEdit.add(p11);
						editFrame.add(pEdit);
						txtMaNV.addKeyListener(new KeyListener() {
							
							@Override
							public void keyTyped(KeyEvent e) {
								// TODO Auto-generated method stub
								
							}
							
							@Override
							public void keyReleased(KeyEvent e) {
								kiemTra = false;
								if(txtMaNV.getText().length() == 5) {
									focus(txtMaNV, "");
									NhanVien nv = dao_nv.traCuuNhanVien(txtMaNV.getText());
									if(nv != null) {
										txtMaNV.setText(nv.getMaNV());
										txtTen.setText(nv.getHoTen());
										txtChucVu.setText(nv.getChucVu());
										if(dao_tk.kiemTraCoTK(txtMaNV.getText()))
											focus(txtMaNV, "Nhân viên này đã có tài khoản");
										else {
											kiemTra = true;
										}
									} else
										focus(txtMaNV, "Không tìm thấy nhân viên phù hợp");
								} else {
										focus(txtMaNV, "Mã nhân viên có 5 ký tự");
								}
							}
							
							@Override
							public void keyPressed(KeyEvent e) {
								
							}
						});
						
						btnThem.addActionListener(new ActionListener() {
							
							@Override
							public void actionPerformed(ActionEvent e) {
									if(kiemTra && regex()) {
										NhanVien nv = dao_nv.traCuuNhanVien(txtMaNV.getText());
										if(dao_tk.taoTK(txtTK.getText(), String.valueOf(txtMK.getPassword()), nv)) {
											tableModel.addRow(new String[] {nv.getMaNV(), txtTK.getText(), nv.getHoTen(), nv.getChucVu()});
											editFrame.dispose();
											JOptionPane.showMessageDialog(QLTKPanel, "Thêm tài khoản thành công", "Thành công", JOptionPane.INFORMATION_MESSAGE);
										} else {
											JOptionPane.showMessageDialog(QLTKPanel, "Thêm tài khoản thất bại", "Thất bại", JOptionPane.ERROR_MESSAGE);
										}
									}
							}
						});
						btnHuy.addActionListener(new ActionListener() {
							
							@Override
							public void actionPerformed(ActionEvent e) {
								// TODO Auto-generated method stub
								editFrame.dispose();
							}
						});
			}
        });
	}
	
	private void updateTableData() {
		List<TaiKhoan> listTK = dao_tk.layDSTaiKhoan();
		List<NhanVien> listNV = dao_nv.layTatCaNhanVien();
		for(TaiKhoan s : listTK)
			for(NhanVien n : listNV) 
				if(s.getMaNV().equals(n.getMaNV())) {
					String[] rowData= {s.getMaNV(),s.getTaiKhoan(), n.getHoTen(), n.getChucVu()};
					tableModel.addRow(rowData);
				}
		table.setModel(tableModel);
	}
	
	public JPanel getDStkPanel() {
		return this.QLTKPanel;
	}
	
	public void focus(JTextField txt, String loi) {
		txt.requestFocus();
		lblRegex.setText(loi);
	}
	
	public boolean regex() {
		String taiKhoan = txtTK.getText();
		String matKhau = String.valueOf(txtMK.getPassword());
		String nMatKhau = String.valueOf(txtNMK.getPassword());
		
		if(taiKhoan.length() > 0) {
			if(!(taiKhoan.matches("^\\w+$"))) {
				focus(txtTK, "Tài khoản gồm chữ và số");
				return false;
			}
		}else {
			focus(txtTK, "Tài khoản không được để trống");
			return false;
		}
		
		if(matKhau.length() >= 8) {
			if(!(matKhau.matches("^\\S+$"))) {
				focus(txtMK, "Tài khoản gồm chữ và số");
				return false;
			}
		}else {
			focus(txtMK, "Mật khẩu tối thiểu 8 ký tự");
			return false;
		}
		if(!matKhau.equals(nMatKhau)) {
			focus(txtNMK, "2 mật khẩu không trùng nhau");
			return false;
		}
		return true;
	}
}
