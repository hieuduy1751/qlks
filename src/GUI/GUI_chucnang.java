package GUI;

import javax.swing.*;

import ChucNang.hamGiaoDien;
import ENTITY.NhanVien;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.font.TextAttribute;
import java.sql.SQLException;
import java.text.DateFormat;
import java.util.Date;
import java.util.Map;
import java.util.TimerTask;
import java.util.Timer;

public class GUI_chucnang implements ActionListener {
	private hamGiaoDien HamGiaoDien = new hamGiaoDien();
	private GUI_DSphong PanelPhong = new GUI_DSphong();
	private GUI_DSNV PanelDSNV = new GUI_DSNV();
	private GUI_DSKH PanelDSKH = new GUI_DSKH();
	private GUI_QLTaiKhoan PanelQLTK = new GUI_QLTaiKhoan();

	private JPanel gui_chucnang = new JPanel(new BorderLayout());
	private NhanVien nv;
	
	//west panel north
	private JLabel lblGio;
	private JLabel lblNgay;
	
	//west panel center
	private JButton btnDSPhong;
	private JButton btnDSKH;
	private JButton btnQLTK;
	private JButton btnQLNV;
	
	//west panel south
	private JLabel lblThongTin;
	private JLabel lblTenNV;
	private JLabel lblChucVu;
	private JButton btnDangXuat;

	//center panel
	private JTabbedPane tabbedPanel;

	public GUI_chucnang(NhanVien n) {
		this.nv = n;
		gui_chucnang.setPreferredSize(new Dimension(1000,700));
		//menu trai
		gui_chucnang.add(WestPanel(), BorderLayout.WEST);
		//bang chuc nang
		gui_chucnang.add(CenterPanel(), BorderLayout.CENTER);
	}
	
	public JPanel getGUI_chucnang() {
		return this.gui_chucnang;
	}
	
	private JPanel WestPanel() {
		JPanel westPanel = new JPanel();
		westPanel.setPreferredSize(new Dimension(300, 700));
		westPanel.setBackground(Color.decode("#c4c4c4"));


		//Ngay gio
		westPanel.add(WestPanelNorth(), BorderLayout.NORTH);
		
		//chuc nang
		westPanel.add(WestPanelCenter(), BorderLayout.CENTER);
		
		//thong tin nhan vien
		westPanel.add(WestPanelSouth(), BorderLayout.SOUTH);
		return westPanel;
	}
	
	private JPanel WestPanelNorth() {
		JPanel wPNorth = new JPanel();
		wPNorth.setPreferredSize(new Dimension(300, 140));
		wPNorth.setBackground(Color.decode("#c4c4c4"));
		
		lblGio = new JLabel();
		lblNgay = new JLabel();
		lblGio.setFont(new Font("Arial", Font.PLAIN, 48));
		lblNgay.setFont(new Font("Arial", Font.PLAIN, 20));
		lblGio.setAlignmentX(JLabel.CENTER_ALIGNMENT);
		lblNgay.setAlignmentX(JLabel.CENTER_ALIGNMENT);
		
		TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                lblGio.setText((new Date().getHours() >= 10 ? "" : "0") + new Date().getHours() + (new Date().getSeconds() % 2 == 0 ? ":" : " ") + (new Date().getMinutes() >= 10 ? "" : "0") + new Date().getMinutes());
                lblNgay.setText("Ngày " + new Date().getDate() + "/" + (new Date().getMonth() >= 10 ? "" : "0") + (new Date().getMonth() + 1) + "/" + (1900 + new Date().getYear()));
            }
        };
        long delay = 1000L;
        Timer timer = new Timer("Timer");
        timer.schedule(timerTask, 0, delay);
		
		Box verticalBox = Box.createVerticalBox();
		verticalBox.setPreferredSize(new Dimension(300, 140));
		verticalBox.add(Box.createVerticalGlue());
		verticalBox.add(lblGio);
		verticalBox.add(lblNgay);
		verticalBox.add(Box.createVerticalGlue());
		wPNorth.add(verticalBox);
		return wPNorth;
	}
	
	private JPanel WestPanelCenter() {
		JPanel wPCenter = new JPanel();
		wPCenter.setPreferredSize(new Dimension(300, 350));
		wPCenter.setBorder(BorderFactory.createEmptyBorder(0, 20, 0, 20));
		wPCenter.setBackground(Color.decode("#c4c4c4"));
		
		btnDSPhong = new JButton("    Danh sách phòng");
		btnDSKH = new JButton("    Danh sách khách hàng");
		btnQLTK = new JButton("    Quản lý tài khoản");
		btnQLNV = new JButton("    Quản lý nhân viên");
		
		btnDSPhong.setFont(new Font("Arial", Font.PLAIN, 20));
		btnDSKH.setFont(new Font("Arial", Font.PLAIN, 20));
		btnQLTK.setFont(new Font("Arial", Font.PLAIN, 20));
		btnQLNV.setFont(new Font("Arial", Font.PLAIN, 20));
		
		btnDSPhong.setHorizontalAlignment(SwingConstants.LEFT);
		btnDSKH.setHorizontalAlignment(SwingConstants.LEFT);
		btnQLTK.setHorizontalAlignment(SwingConstants.LEFT);
		btnQLNV.setHorizontalAlignment(SwingConstants.LEFT);
		
		btnDSPhong.setPreferredSize(new Dimension(300, 40));
		btnDSKH.setPreferredSize(new Dimension(300, 40));
		btnQLTK.setPreferredSize(new Dimension(300, 40));
		btnQLNV.setPreferredSize(new Dimension(300, 40));
		
		this.HamGiaoDien.datButtonThanhLabel(btnDSPhong);
		this.HamGiaoDien.datButtonThanhLabel(btnDSKH);
		this.HamGiaoDien.datButtonThanhLabel(btnQLTK);
		this.HamGiaoDien.datButtonThanhLabel(btnQLNV);
		
		btnDSPhong.setBorder(BorderFactory.createMatteBorder(1, 0, 0, 0, Color.GRAY));
		btnDSKH.setBorder(BorderFactory.createMatteBorder(1, 0, 0, 0, Color.GRAY));
		btnQLTK.setBorder(BorderFactory.createMatteBorder(1, 0, 0, 0, Color.GRAY));
		btnQLNV.setBorder(BorderFactory.createMatteBorder(1, 0, 0, 0, Color.GRAY));	
		
		wPCenter.add(btnDSPhong);
		wPCenter.add(btnDSKH);
		wPCenter.add(btnQLTK);
		wPCenter.add(btnQLNV);
		btnDSPhong.setBackground(Color.WHITE);
		btnDSPhong.addActionListener(this);
		btnDSKH.addActionListener(this);
		btnQLTK.addActionListener(this);
		btnQLNV.addActionListener(this);
		
		return wPCenter;
	}
	
	private JPanel WestPanelSouth() {
		JPanel wPSouth = new JPanel();
		wPSouth.setPreferredSize(new Dimension(300, 150));
		wPSouth.setBackground(Color.decode("#c4c4c4"));
		
		lblThongTin = new JLabel("THÔNG TIN");
		lblTenNV = new JLabel(nv.getHoTen());
		lblChucVu = new JLabel(nv.getChucVu());
		btnDangXuat = new JButton("Đăng xuất");
		
		lblThongTin.setFont(new Font("Arial", Font.BOLD, 24));
		lblTenNV.setFont(new Font("Arial", Font.PLAIN, 20));
		lblChucVu.setFont(new Font("Arial", Font.PLAIN, 15));
		lblThongTin.setAlignmentX(JLabel.CENTER_ALIGNMENT);
		lblTenNV.setAlignmentX(JLabel.CENTER_ALIGNMENT);
		lblChucVu.setAlignmentX(JLabel.CENTER_ALIGNMENT);
		
		btnDangXuat.setFont(new Font("Arial", Font.PLAIN, 15));
		this.HamGiaoDien.datChuGachChan(btnDangXuat);
		this.HamGiaoDien.datButtonThanhLabel(btnDangXuat);
		btnDangXuat.setBorderPainted(false);
		btnDangXuat.setAlignmentX(JButton.CENTER_ALIGNMENT);
		btnDangXuat.setForeground(Color.decode("#0047FF"));
		
		
		Box verticalBox = Box.createVerticalBox();
		verticalBox.setPreferredSize(new Dimension(300, 150));
		verticalBox.add(lblThongTin);
		verticalBox.add(lblTenNV);
		verticalBox.add(lblChucVu);
		verticalBox.add(Box.createVerticalGlue());
		verticalBox.add(btnDangXuat);
		verticalBox.add(Box.createVerticalGlue());
		
		wPSouth.add(verticalBox);
		return wPSouth;
	}
	
	private JPanel CenterPanel() {
		JPanel centerPanel = new JPanel(null);
		tabbedPanel = new JTabbedPane();
		tabbedPanel.setBounds(-10, -30, 700, 700);
		tabbedPanel.add("tab1", this.PanelPhong.getDSphongPanel());
		tabbedPanel.add("tab2", this.PanelDSKH.getDSkhPanel());
		tabbedPanel.add("tab3", this.PanelQLTK.getDStkPanel());
		tabbedPanel.add("tab4", this.PanelDSNV.getDSnvPanel());
		
		centerPanel.add(tabbedPanel);
		return centerPanel;
	}
	
	public void tickTock() {
        lblGio.setText(DateFormat.getDateTimeInstance().format(new Date()));
    }
	
	public void setNV(NhanVien nhanVien) {
		this.nv = nhanVien;
	}
	
	public NhanVien getNV() {
		return this.nv;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Object source = e.getSource();
		if(source.equals(btnDSPhong)) {
			tabbedPanel.setSelectedIndex(0);;
			btnDSPhong.setBackground(Color.WHITE);
			btnDSKH.setBackground(Color.decode("#c4c4c4"));
			btnQLTK.setBackground(Color.decode("#c4c4c4"));
			btnQLNV.setBackground(Color.decode("#c4c4c4"));
		}
		
		if(source.equals(btnDSKH)) {
			tabbedPanel.setSelectedIndex(1);
			btnDSKH.setBackground(Color.WHITE);
			btnQLNV.setBackground(Color.decode("#c4c4c4"));
			btnQLTK.setBackground(Color.decode("#c4c4c4"));
			btnDSPhong.setBackground(Color.decode("#c4c4c4"));
		}
		
		if(source.equals(btnQLTK)) {
			tabbedPanel.setSelectedIndex(2);
			btnQLTK.setBackground(Color.WHITE);
			btnQLNV.setBackground(Color.decode("#c4c4c4"));
			btnDSKH.setBackground(Color.decode("#c4c4c4"));
			btnDSPhong.setBackground(Color.decode("#c4c4c4"));
		}
		
		
		if(source.equals(btnQLNV)) {
			tabbedPanel.setSelectedIndex(3);
			btnQLNV.setBackground(Color.WHITE);
			btnDSKH.setBackground(Color.decode("#c4c4c4"));
			btnQLTK.setBackground(Color.decode("#c4c4c4"));
			btnDSPhong.setBackground(Color.decode("#c4c4c4"));
		}
		
		
	}
}
