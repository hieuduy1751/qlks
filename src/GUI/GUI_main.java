package GUI;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import javax.swing.JFrame;

import ENTITY.NhanVien;
import connection.ConnectDB;

public class GUI_main extends JFrame implements ActionListener {
	private GUI_chucnang gui_chucnang;
	private GUI_DangNhap gui_DangNhap = new GUI_DangNhap();
	public GUI_main() {
		setTitle("Quản lý khách sạn");
		setSize(1000, 700);
//		setResizable(false);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		try {
			ConnectDB.getInstance().connect();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.add(gui_DangNhap.getGUIdangNhap());
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new GUI_main().setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
		if(this.gui_DangNhap.getNV() != null) {
			this.gui_chucnang = new GUI_chucnang(this.gui_DangNhap.getNV());
			this.remove(gui_DangNhap.getGUIdangNhap());
			this.add(gui_chucnang.getGUI_chucnang());
		}
	}

}
